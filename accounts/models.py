from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, AbstractBaseUser

# Create your models here.


class Author(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    email = models.EmailField(unique=True)
    points = models.PositiveIntegerField(default=0)
    is_anonymous = models.BooleanField(default=True)
    is_authenticated = models.BooleanField(default=False)

    REQUIRED_FIELDS = ('user',)
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email
