from django.contrib import admin
from .models import Request, Feature


admin.site.register(Request)
admin.site.register(Feature)
