from django import forms
from .models import Feature, Request


class ContactForm(forms.ModelForm):
    class Meta:
        model = Request
        fields = '__all__'


class FeatureForm(forms.ModelForm):
    class Meta:
        model = Feature
        fields = '__all__'
