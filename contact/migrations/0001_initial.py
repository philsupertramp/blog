# Generated by Django 2.0.5 on 2018-05-12 12:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender', models.EmailField(max_length=254)),
                ('feature_type', models.CharField(choices=[('Add-On', 'Add-On'), ('Code', 'Code'), ('Site', 'Site'), ('Tool', 'Tool')], max_length=6)),
                ('comment', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sender', models.EmailField(max_length=254)),
                ('request', models.TextField()),
            ],
        ),
    ]
