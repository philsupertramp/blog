from django.db import models
from django.core.mail import send_mail
from django.conf import settings


class Request(models.Model):
    sender = models.EmailField()
    request = models.TextField()

    def __str__(self):
        return 'Request #{0} from {1}'.format(self.pk, self.sender)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        body = 'Contact request:\n{0}'.format(self.request)
        send_mail('Request #{0} from {1}'.format(self.pk + 10, self.sender),
                  body,
                  from_email='blog_contact@time-dev.de',
                  recipient_list=[settings.EMAIL_HOST_USER, ]
                  )


class Feature(models.Model):
    FEATURE_ADDON = 'Add-On'
    FEATURE_CODE = 'Code'
    FEATURE_SITE = 'Site'
    FEATURE_TOOL = 'Tool'
    FEATURE_CHOICES = (
        ('Add-On', FEATURE_ADDON),
        ('Code', FEATURE_CODE),
        ('Site', FEATURE_SITE),
        ('Tool', FEATURE_TOOL)
    )
    sender = models.EmailField()
    feature_type = models.CharField(max_length=6, choices=FEATURE_CHOICES)
    comment = models.TextField()

    def __str__(self):
        return 'Feature request #{0} type: {1} from {2}'.format(self.pk, self.feature_type, self.sender)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        send_mail('Request #{0} from {1} type {2}'.format(self.pk+10 if not settings.DEBUG else self.pk, self.sender, self.feature_type),
                  self.comment,
                  from_email='blog_request@time-dev.de',
                  recipient_list=[settings.GITLAB_SERVICEDESK, ]
                  )
