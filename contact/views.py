from django.shortcuts import render
from .forms import ContactForm, FeatureForm
from django.contrib import messages
from post.views import basic_components


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact_request = form.save(commit=False)
            contact_request.save()
            messages.success(request, 'Contact request submitted')
            return render(request, 'contact/request.html', basic_components())
    else:
        form = ContactForm()
    return render(request, 'contact/request.html', basic_components({'form': form}))


def feature(request):
    if request.method == 'POST':
        form = FeatureForm(request.POST)
        if form.is_valid():
            feature_request = form.save(commit=False)
            feature_request.save()
            messages.success(request, 'Feature request submitted')
            return render(request, 'contact/feature.html', basic_components())
    else:
        form = FeatureForm()
    return render(request, 'contact/feature.html', basic_components({'form': form}))
