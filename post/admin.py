# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Post, Tag, Author, Group


class PostAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                ('title', 'tags',),
                ('author',),
                ('content',)
            )
        }),
    )
    raw_id_fields = ('tags', 'author')
    autocomplete_lookup_fields = {
        'm2m': ['tags', 'author']
    }


admin.site.register(Author)
admin.site.register(Post, PostAdmin)
admin.site.register(Tag)
admin.site.register(Group)
