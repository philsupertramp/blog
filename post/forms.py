from markdownx.fields import MarkdownxFormField
from django import forms
from .models import Post


class PostForm(forms.ModelForm):
    content = MarkdownxFormField()

    class Meta:
        model = Post
        exclude = ['author', ]
