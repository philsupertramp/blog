"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^tags/(?P<tag>[\w+]+)/$', views.tags, name='tags'),
    url(r'^find/$', views.find, name='find'),
    url(r'post/$', views.PostCreate.as_view(), name='post-create'),
    url(r'post/(?P<pk>[\w+]+)/$', views.PostDetail.as_view(), name='post-detail'),
    url(r'edit/(?P<pk>[\w+]+)/$', views.PostUpdate.as_view(), name='post-update'),
]
