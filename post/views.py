# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from django.views.generic import UpdateView, DetailView, CreateView
from .models import Tag, Post, Group
from .forms import PostForm
from accounts.models import Author
# Create your views here.


def basic_components(*args):
    tags = Tag.objects.all()
    groups = Group.objects.all()
    content = {'tags': tags, 'groups': groups}
    if args:
        q = 0
        for i in args:
            for j in i:
                c = list(i.keys())
                content[c[q]] = i[j]
                q += 1
        return content


def home(request):
    post = Post.objects.all().order_by('-pk')[:5]
    base = basic_components({'post': post})
    return render(request, 'general/home.html', context=base)


def tags(request, tag):
    tag = Tag.objects.filter(name=tag)
    posts = Post.objects.filter(tags__in=tag)

    content = {'tag': tag, 'posts': posts}
    base = basic_components(content)
    return render(request, 'tags/listing.html', context=base)


def find(request):
    query = request.POST.get('find')
    if request.method == 'POST':
        if query is not '':
            post = Post.objects.filter(title__contains=query)
            content = {'post': post, 'query': query}
            base = basic_components(content)
            return render(request, 'post/find.html', context=base)
    base = basic_components()
    return render(request, 'post/find.html', context=base)


class PostDetail(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = basic_components(context)
        return context


class PostUpdate(UpdateView):
    model = Post
    template_name_suffix = '_edit'

    form_class = PostForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = basic_components(context)
        return context

    def get_success_url(self):
        return reverse('post-detail', kwargs={'pk': self.kwargs['pk'], })


class PostCreate(CreateView):
    model = Post
    template_name_suffix = '_create'
    fields = ['title', 'content', 'tags']

    def form_valid(self, form):
        form.instance.author = Author.objects.get(user__username=self.request.user.username)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context = basic_components(context)
        return context

    def get_success_url(self):
        return reverse('post-detail', kwargs={'pk': self.object.pk, })
