Django==2.0.5
django-grappelli==2.11.1
django-markdownx==2.0.23
Markdown==2.6.11
Pillow==5.1.0
pytz==2018.4
uWSGI==2.0.17
